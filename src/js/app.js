import * as flsFunctions from "./modules/functions.js";

// flsFunctions.isWebp();
flsFunctions.BlogPosts();
flsFunctions.useDynamicAdapt();

import Swiper, { Navigation, Autoplay, Scrollbar, Lazy } from 'swiper';


let body = document.body;
let modalWrapper = document.querySelector('.modal-wrapper');



//* ===========BLOG-BURGER-MENU ============

function blogBurgerMenu() {

  let blogBurgerBtn = document.querySelector('.header-blog__burger-btn');
  let blogHeader = document.querySelector('.header-blog');

  if (blogHeader) {

    blogBurgerBtn.addEventListener('click', function () {
      blogHeader.classList.toggle('active');
      body.classList.toggle('active');
    });
  };
};
blogBurgerMenu();


//* ===========BTNs-PREVENT-DEFAULT ============

function BtnPrDef() {

  const BtnsNotLink = document.querySelectorAll('.nft-card__btn, .bag-item__remove, .bag-item__remove-mini');

  BtnsNotLink.forEach((Item) => Item.addEventListener('click', (e) =>
    e.preventDefault())
  );
};
BtnPrDef();

//* ===========BODY-POINTER-EVENTS-NONE ============

function hoverDelay() {

  const ENABLE_HOVER_DELAY = 150;
  let timer;
  window.addEventListener('scroll', function () {
    const bodyClassList = document.body.classList;
    clearTimeout(timer);

    if (!bodyClassList.contains('disable-hover')) {
      bodyClassList.add('disable-hover');
    }

    timer = setTimeout(function () {
      bodyClassList.remove('disable-hover');
    }, ENABLE_HOVER_DELAY);

  }, false);
};
hoverDelay();

//* =========  SLIDERS  ===========

function initSliders() {
  const swiperHero = new Swiper('.slider-hero__container', {

    modules: [Navigation, Autoplay, Scrollbar],
    slidesPerView: 1,
    loop: false,
    speed: 500,
    shortSwipes: false,
    longSwipesMs: 1,
    longSwipesRatio: 0.05,
    scrollbar: {
      el: '.slider-hero__scrollbar',
      draggable: true,
    },
    navigation: {
      nextEl: '.slider-hero__next',
      prevEl: '.slider-hero__prev',
    },
    autoplay: {
      delay: 4000,
    },
    on: {
      slideChange: function () {

        let heroInner = document.querySelector('.hero__inner');
        let activeSlides = heroInner.querySelectorAll('.swiper-slide-active');
        activeSlides.forEach(item => {

          if (item.classList.contains('slide-4')) {
            heroInner.classList.add('bg-slide-1');
            heroInner.classList.remove('bg-slide-4');
          }
        })
      },
      slideNextTransitionStart: function () {

        let heroInner = document.querySelector('.hero__inner');
        let activeSlides = heroInner.querySelectorAll('.swiper-slide-active');

        activeSlides.forEach(item => {

          if (item.classList.contains('slide-2')) {
            heroInner.classList.add('bg-slide-2');
            heroInner.classList.remove('bg-slide-1');
          }
          if (item.classList.contains('slide-3')) {
            heroInner.classList.add('bg-slide-3');
            heroInner.classList.remove('bg-slide-2');
          }
          if (item.classList.contains('slide-4')) {
            heroInner.classList.add('bg-slide-4');
            heroInner.classList.remove('bg-slide-3');
          }
        })
      },

      slidePrevTransitionStart: function () {
        let heroInner = document.querySelector('.hero__inner');
        let activeSlides = heroInner.querySelectorAll('.swiper-slide-active');

        activeSlides.forEach(item => {

          if (item.classList.contains('slide-1')) {
            heroInner.classList.add('bg-slide-1');
            heroInner.classList.remove('bg-slide-2');
          }

          if (item.classList.contains('slide-2')) {
            heroInner.classList.add('bg-slide-2');
            heroInner.classList.remove('bg-slide-3');
          }
          if (item.classList.contains('slide-3')) {
            heroInner.classList.add('bg-slide-3');
            heroInner.classList.remove('bg-slide-4');
            heroInner.classList.remove('bg-slide-1');
          }
        });
      }

    }
  });

  const swiperCollection = new Swiper('.top-collections-slider__container', {
    modules: [Navigation, Autoplay],
    slidesPerView: 3,
    slidesPerGroup: 1,
    spaceBetween: 15,
    loop: true,
    speed: 500,

    navigation: {
      nextEl: '.top-collections__next',
      prevEl: '.top-collections__prev',
    },

    breakpoints: {
      0: {
        slidesPerView: 1
      },

      660: {
        slidesPerView: 2
      },

      1024: {
        slidesPerView: 3
      }
    }
  });

  const swiperMarket = new Swiper('.marketplace-slider__container', {
    modules: [Navigation, Autoplay, Lazy],
    spaceBetween: 15,
    speed: 700,
    lazy: {
      enabled: true,
      loadOnTransitionStart: true,
    },
    loop: true,
    loopedSlides: 0,
    shortSwipes: false,
    longSwipesMs: 1,
    longSwipesRatio: 0.05,
    navigation: {
      nextEl: '.marketplace-slider__next',
      prevEl: '.marketplace-slider__prev',
    },

    breakpoints: {

      0: {
        slidesPerView: 2,
        slidesPerGroup: 2,
      },

      480: {
        slidesPerView: 3,
        slidesPerGroup: 3,
      },

      660: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },


      768: {
        slidesPerView: 3,
        slidesPerGroup: 3,
      },

      1200: {
        slidesPerView: 4,
        slidesPerGroup: 4
      },

      1440: {
        slidesPerView: 5,
        slidesPerGroup: 5
      }
    }
  });

  const swiperNews = new Swiper('.news-slider__container', {
    modules: [Navigation, Autoplay],
    slidesPerView: 3,
    slidesPerColumn: 1,
    spaceBetween: 15,
    loop: true,
    speed: 500,
    navigation: {
      nextEl: '.news-slider__next',
      prevEl: '.news-slider__prev',
    },

    breakpoints: {
      0: {
        slidesPerView: 1,
        slidesPerGroup: 1,
      },

      540: {
        slidesPerView: 2,
        slidesPerGroup: 1,

      },

      1024: {
        slidesPerView: 3,
        slidesPerGroup: 1,

      },

    }

  });

  const swiperToken = new Swiper('.token-slider__container', {
    modules: [Navigation, Scrollbar],
    slidesPerView: 5,
    slidesPerGroup: 5,
    spaceBetween: 15,
    loop: false,
    speed: 700,
    shortSwipes: false,
    longSwipesMs: 1,
    longSwipesRatio: 0.05,
    scrollbar: {
      el: '.token-slider__scrollbar',
      draggable: true,
    },
    navigation: {
      nextEl: '.token-slider__next',
      prevEl: '.token-slider__prev',
    },
    autoplay: {
      delay: 4000,
    },
    breakpoints: {
      0: {
        slidesPerView: 2,
        slidesPerGroup: 2,
      },

      480: {
        slidesPerView: 3,
        slidesPerGroup: 3,
      },

      660: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },


      768: {
        slidesPerView: 3,
        slidesPerGroup: 3,
      },

      1200: {
        slidesPerView: 4,
        slidesPerGroup: 4
      },

      1440: {
        slidesPerView: 5,
        slidesPerGroup: 5
      }
    }
  });


  let slider = document.querySelectorAll('.slider');
  
  slider.forEach(item => {
     
    let sliderContainer = item.querySelector('.swiper-container');
    let sliderPreloader = item.querySelector('.slider-preloader');

    if (sliderContainer.classList.contains('swiper-initialized')) {
  
      sliderPreloader.remove();
      sliderContainer.style.cssText= `
      opacity: 1;
      height: 100%;
      padding: 10px 2px;
  `
  }

    });

}
initSliders();



//* ========= LINK-TO-WALLET  ===========

function redirectLinkstoWallet() {
  let linksToWallet = document.querySelectorAll('.sell-link, .create-link');
  let walletInner = document.querySelector('.wallet__inner');

  linksToWallet.forEach(item => {
    item.addEventListener('click', (e) => {
      e.preventDefault();
      if (modalWrapper.classList.contains('active-burger')) {
        modalWrapper.classList.remove('active-burger');
        burgerInner.classList.remove('active');
      };
      modalWrapper.classList.add('active-wallet');
      walletInner.classList.add('active');
    });
  });
}
redirectLinkstoWallet();

//* ========= THEME-SWITCHER  ===========

function ThemeSwitcher() {

  let html = document.getElementsByTagName('html')[0];
  let themeSwitcher = document.querySelector('.theme-switcher');

  themeSwitcher.addEventListener('click', function () {
    if (html.classList.contains('theme-light')) {
      html.classList.remove('theme-light');
      themeSwitcher.classList.remove('icon-dark');
      themeSwitcher.classList.add('icon-light');
      localStorage.setItem('theme', 'dark');
    }
    else {
      html.classList.add('theme-light');
      themeSwitcher.classList.add('icon-dark');
      themeSwitcher.classList.remove('icon-light');
      localStorage.setItem('theme', 'light');
    }
  });

  if (localStorage.getItem('theme') === 'light') {
    themeSwitcher.classList.remove('icon-light');
    themeSwitcher.classList.add('icon-dark');
  }
}
ThemeSwitcher();

//* ========= ADP-SEARCH  ===========
function adpSearch() {
  let adpSearchBtnOpen = document.querySelector('.header-search-btn');
  let adpSearchBtnClose = document.querySelector('.adp-search__close-btn');
  let adpSearch = document.querySelector('.adp-search');

  adpSearchBtnOpen.addEventListener('click', () => {
    adpSearch.classList.add('active');
  });

  adpSearchBtnClose.addEventListener('click', () => {
    adpSearch.classList.remove('active');
  });
}
adpSearch();

//* =========  BAG  ===========

function Bag() {
  let bag = document.querySelector('.bag');
  let walletInner = document.querySelector('.wallet__inner');
  if (bag) {

    let bagBtnToggle = document.querySelectorAll('.header__cart, .top-bag__close');
    let bagCheckOutBtn = document.querySelector('.footer-bag__check');

    bagBtnToggle.forEach(item => {
      item.addEventListener('click', () => {
        modalWrapper.classList.toggle('active-bag');
        bag.classList.toggle('active');
      });
    });

    bagCheckOutBtn.addEventListener('click', () => {
      bag.classList.remove('active');
      modalWrapper.classList.remove('active-bag');
      modalWrapper.classList.add('active-wallet');
      walletInner.classList.add('active');
      body.classList.add('active');
    });
  }
}
Bag();

//* =========  WALLET  ===========
function Wallet() {

  let walletInner = document.querySelector('.wallet__inner');
  if (walletInner) {

    let bag = document.querySelector('.bag');
    let walletBtn = document.querySelector('.header__wallet');
    let walletBtnMore = document.querySelector('.wallet__show-more');
    let walletBoxMore = document.querySelector('.wallet-box--more');
    let walletCloseBtns = document.querySelectorAll('.wallet__close-btn, .wallet__cancel');

    walletBtn.addEventListener('click', () => {
      if (modalWrapper.classList.contains('active-bag')) {
        modalWrapper.classList.remove('active-bag');
        bag.classList.remove('active');
      };
      modalWrapper.classList.add('active-wallet');
      walletInner.classList.add('active');
      body.classList.add('active');
    });

    walletBtnMore.addEventListener('click', function () {
      walletBtnMore.innerHTML =
        (walletBtnMore.innerHTML === 'Show less') ? walletBtnMore.innerHTML = 'Show more' : walletBtnMore.innerHTML = 'Show less';
      walletBoxMore.classList.toggle('active');
    });


    walletCloseBtns.forEach(item => {
      item.addEventListener('click', () => {
        modalWrapper.classList.remove('active-wallet');
        walletInner.classList.remove('active');
        body.classList.remove('active');
      });
    });

    document.addEventListener('click', (e) => {
      if (modalWrapper.classList.contains('active-wallet')) {
        if (e.target === modalWrapper) {
          modalWrapper.classList.remove('active-wallet');
          walletInner.classList.remove('active');
          body.classList.remove('active');
        }
      }
    });
  }

};
Wallet();

//* =========  BURGER-MENU ===========

function burgerMenu() {
  let bag = document.querySelector('.bag');
  let walletInner = document.querySelector('.wallet__inner');
  let burgerInner = document.querySelector('.burger-menu__inner');

  if (burgerInner) {

    let openBurgerBtn = document.querySelector('.header__burger-btn');
    let closeBurgerBtn = document.querySelector('.burger-menu__close');
    let connectBurgerBtn = document.querySelector('.burger-menu__connect-btn');

    openBurgerBtn.addEventListener('click', () => {
      if (modalWrapper.classList.contains('active-bag')) {
        modalWrapper.classList.remove('active-bag');
        bag.classList.remove('active');
      }
      modalWrapper.classList.add('active-burger');
      burgerInner.classList.add('active');
      body.classList.add('active');
    });
    closeBurgerBtn.addEventListener('click', () => {
      modalWrapper.classList.remove('active-burger');
      burgerInner.classList.remove('active');
      body.classList.remove('active');
    });
    connectBurgerBtn.addEventListener('click', () => {
      modalWrapper.classList.remove('active-burger');
      burgerInner.classList.remove('active');
      modalWrapper.classList.add('active-wallet');
      walletInner.classList.add('active');
    });
    document.addEventListener('click', (e) => {
      if (e.target === modalWrapper) {
        modalWrapper.classList.remove('active-burger');
        burgerInner.classList.remove('active');
        body.classList.remove('active');
      }
    });
  }
}
burgerMenu();

//! =========  TABS  ===========
function tabs() {
  
  let tabsWrapper = document.querySelectorAll('.tabs-wrapper');
  if (tabsWrapper) {
  
    tabsWrapper.forEach((e) => {
    
      let tabsMenuItem = e.querySelectorAll('.tabs-menu__item');
      let tabsContentTab = e.querySelectorAll('.tabs-content__tab');
      let tabMenuBtnTokenBids = e.querySelector('.token-bids__btn');
    
      for (let i = 0; i < tabsMenuItem.length; i++) {
        // tabsMenuItem[0].click();
        tabsMenuItem[i].onclick = () => {
          tabsMenuItem.forEach((e) => { e.classList.remove('active') });
          tabsContentTab.forEach((e) => { e.classList.remove('active') });
          tabsMenuItem[i].classList.add('active');
          tabsContentTab[i].classList.add('active');
        }
      }
    
      tabMenuBtnTokenBids.addEventListener('click', () => {
        let tabsMenuItemActive = document.querySelector('.tabs-menu__item.active');
        let tabsContentTabActive = document.querySelector('.tabs-content__tab.active');
  
        tabsMenuItemActive.classList.remove('active');
        tabsContentTabActive.classList.remove('active');
        tabsMenuItemActive.nextElementSibling.classList.add('active');
        tabsContentTabActive.nextElementSibling.classList.add('active');
    
      })
    });
  }
}
tabs();

//* =========  DROPDOWN ===========
function dropDown() {
  const dropDown = document.querySelectorAll('.dropdown');
  if (dropDown) {
    dropDown.forEach(function (dropDownWrapper) {

      const dropDownBtn = dropDownWrapper.querySelector('.dropdown__btn');
      const dropDownBody = dropDownWrapper.querySelector('.dropdown__body');
      const dropDownItems = dropDownBody.querySelectorAll('.dropdown__item');
      const dropDownCurrent = dropDownWrapper.querySelector('.dropdown__current');
      const dropDownIcon = dropDownWrapper.querySelector('.dropdown__icon');
      const dropDownInput = dropDownWrapper.querySelector('.dropdown__input');

      dropDownBody.addEventListener('click', function (e) {
        e.stopPropagation();
      });

      dropDownBtn.addEventListener('click', function () {
        dropDownBody.classList.toggle('active');
        if (dropDownIcon) {
          dropDownIcon.classList.toggle('active');
        }
      });

      dropDownItems.forEach(function (item) {
        item.addEventListener('click', function () {
          dropDownCurrent.innerText = this.innerText;
          Array.from(dropDownItems).forEach(item => {
            item.classList.remove('active')
          });
          this.classList.add('active');

          dropDownInput.value = this.dataset.value;
          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        })
      });

      document.addEventListener('click', function (e) {

        if (e.target !== dropDownBtn) {

          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        }
      });

      document.addEventListener('keydown', function (e) {
        if (e.key === 'Tab' || e.key === 'Escape') {
          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        }
      });

    });
  }
}
dropDown();

//* =========  FILTER ===========

function assetsFilter() {
  let assetsFilter = document.querySelector('.assets-filter');

  if (assetsFilter) {
    let assetsFilterItemTop = document.querySelectorAll('.assets-filter-item__top');
    let assetsFilterItemMain = document.querySelectorAll('.assets-filter-item__main');
    let assetsFilterShowBtn = document.querySelector('.assets-top__filter-btn');
    let assetsFilterBtnClose = document.querySelector('.assets-filter-header__btn-close');
    let assetsFilterFooterBtn = document.querySelectorAll('.assets-filter-footer__btn');
    let assetsContent = document.querySelector('.assets-content');

    assetsFilterItemTop.forEach(item => {
      item.addEventListener('click', itemHandler);
    });

    function itemHandler(e) {
      e.preventDefault();
      let currentItemBtn = e.target
      let currentContent = e.target.nextElementSibling;

      currentItemBtn.classList.toggle('active');

      if (currentItemBtn.classList.contains('active')) {
        currentContent.classList.add('active');
      }
      else {
        currentContent.classList.remove('active')
      }

    };

    assetsFilterItemMain.forEach(function (assetsFilterItemWrapper) {

      let assetsFilterItemBtn = assetsFilterItemWrapper.querySelectorAll('.assets-filter-item__btn');

      assetsFilterItemBtn.forEach(function (item) {
        item.addEventListener('click', function () {

          let assetsFilterItemBtnActive = item;

          if (!assetsFilterItemBtnActive.classList.contains('btn--second')) {

            assetsFilterItemBtn.forEach(function (item) {
              item.classList.remove('btn--second');
            });

            assetsFilterItemBtnActive.classList.add('btn--second');

          }
        });
      });
    });

    assetsFilterShowBtn.addEventListener('click', function () {

      if (window.matchMedia("(min-width: 965px)").matches) {
        assetsFilterShowBtn.classList.toggle('active');
        assetsFilter.classList.toggle('active');
        assetsContent.classList.toggle('active');

      }

      if (window.matchMedia("(max-width: 964.98px)").matches) {
        modalWrapper.classList.add('active-filter');
        assetsFilter.classList.add('active');
        body.classList.add('active');

        document.addEventListener('click', (e) => {
          if (e.target === modalWrapper) {
            modalWrapper.classList.remove('active-filter');
            assetsFilter.classList.remove('active');
            body.classList.remove('active');
          }
        });
      }

    });

    assetsFilterBtnClose.addEventListener('click', () => {
      modalWrapper.classList.remove('active-filter');
      assetsFilter.classList.remove('active');
      body.classList.remove('active');
    });

    assetsFilterFooterBtn.forEach(function (item) {
      item.addEventListener('click', function () {

        modalWrapper.classList.remove('active-filter');
        assetsFilter.classList.remove('active');
        body.classList.remove('active');

      });
    });

    if (window.matchMedia("(max-width: 964.98px)").matches) {
      assetsFilterShowBtn.classList.remove('active');
      assetsFilter.classList.remove('active');
      assetsContent.classList.add('active');
    };

    window.addEventListener('resize', function () {

      if (window.matchMedia("(max-width: 964.98px)").matches) {
        assetsFilterShowBtn.classList.remove('active');
        assetsContent.classList.add('active');
      }
      else {
        assetsFilterShowBtn.classList.add('active');
      }

      if (window.matchMedia("(min-width: 965px)").matches) {
        modalWrapper.classList.remove('active-filter');
        body.classList.remove('active');
        if (assetsFilter.classList.contains('active')) {
          assetsContent.classList.remove('active');
        }
        else {
          assetsContent.classList.add('active')
        }
      }

    });
  }
}
assetsFilter();
